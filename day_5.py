import argparse
from typing import TextIO
import math


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def add(self, rhs):
        return Point(round(self.x + rhs.x), round(self.y + rhs.y))

    def sub(self, rhs):
        return Point(round(self.x - rhs.x), round(self.y - rhs.y))

    def scalar_div(self, scalar):
        return Point(self.x / scalar, self.y / scalar)

    def magnitude(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))

    def __repr__(self):
        return f'({self.x}, {self.y})'


class Line:
    def __init__(self, text):
        p1, p2 = text.split('->')
        p1 = tuple([float(x) for x in p1.split(',')])
        self.p1 = Point(p1[0], p1[1])
        p2 = tuple([float(x) for x in p2.split(',')])
        self.p2 = Point(p2[0], p2[1])

    def is_horizontal_or_vertical(self):
        return self.p1.x == self.p2.x or self.p1.y == self.p2.y

    def get_points(self):
        target = self.p2.sub(self.p1)
        unit_vector = target.scalar_div(target.magnitude())

        point = self.p1
        points = [point]
        while point != self.p2:
            point = point.add(unit_vector)
            points.append(point)
        return points

    def __repr__(self):
        return f'{self.p1} -> {self.p2}'


def part_1(input_file: TextIO):
    lines = []
    for text_line in input_file.readlines():
        lines.append(Line(text_line))
    lines = [x for x in lines if x.is_horizontal_or_vertical()]

    grid = {}
    for line in lines:
        for point in line.get_points():
            if point in grid:
                grid[point] += 1
            else:
                grid[point] = 1

    num_intersecting_points = sum(1 if x > 1 else 0 for x in grid.values())
    return num_intersecting_points


def part_2(input_file: TextIO):
    lines = []
    for text_line in input_file.readlines():
        lines.append(Line(text_line))

    grid = {}
    for line in lines:
        for point in line.get_points():
            if point in grid:
                grid[point] += 1
            else:
                grid[point] = 1

    num_intersecting_points = sum(1 if x > 1 else 0 for x in grid.values())
    return num_intersecting_points


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 4')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
